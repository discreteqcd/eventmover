# EventMover: Move the data -- not the weights! 

<img src="./docs/illustration.jpeg?raw=true" alt="EventMover Illustration" title="Don't rescale - move the sample." width="500">

Welcome to EventMover, a differentiable parton shower event generator to simulate the evolution of high-energy particles produced in ultra-relativistic particle collisions. EventMover is an unconventional parton shower based on the [Discrete-QCD method](https://inspirehep.net/literature/404514), and employs automatic differentiation to morph events simulated with one set of parameters to events for arbitrary new parameter sets.

## Requirements

The code is written in Python 3 and uses JAX for automatic differentiation -- so make sure these are set up correctly.

## Running the code

You can run the code my executing the ``shower.py`` script. Try `shower.py --help` to get started!

## Authors and acknowledgment

EventMover has been developed by Benjamin Nachman and Stefan Prestel, see the [preprint arXiv: ](https://arxiv.org/) for details. Feel free to get in touch with Stefan about the implementation!

## License

EventMover is licenced under the GNU GPL v2 or later.
