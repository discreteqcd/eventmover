import xml.etree.ElementTree as ElementTree
import xml.dom.minidom as minidom
from math import sqrt
import copy

def LHEFExcept(Exception):
    def __init__(self,message):
        super(LHEFExcept,self).__init__(message)
        self.message = message

class LHAweights:
    def __init__(self):
        self.weights = []
        self.attrib = {}
        self.text = ""

    def FromXML(self,element):
        if element.tag != "weights":
            raise Exception("Cannot construct LHAweights from: {0}".format(element.tag))
        for key, value in element.attrib.items():
            self.attrib[key] = value

        self.text = element.text
        self.weights = self.text.split()

    def ToXML(self,root):
        xml = ElementTree.SubElement(root,"weights")
        for key, value in self.attrib.items():
            xml.attrib[key] = str(value)
        xml.text = " ".join(self.weights)
        return xml

class LHAscales:
    def __init__(self):
        self.muf = -1
        self.mur = -1
        self.mups = -1
        self.SCALUP = -1
        self.attrib = {}
        self.text = ""

    def FromXML(self,element):
        if element.tag != "scales":
            raise Exception("Cannot construct LHAscales from: {0}".format(element.tag))
        for key, value in element.attrib.items():
            if key == "muf": self.muf = value
            elif key == "mur": self.mur = value
            elif key == "mups": self.mups = value
            else: self.attrib[key] = value
        self.text = element.text

    def ToXML(self,root):
        xml = ElementTree.SubElement(root,"scales")
        xml.attrib["muf"] = self.muf
        xml.attrib["mur"] = self.mur
        xml.attrib["mups"] = self.mups
        for key, value in self.attrib.items():
            xml.attrib[key] = str(value)
        xml.text = str(self.text)
        return xml

class LHAgenerator:
    def __init__(self):
        self.name = ""
        self.version = ""
        self.text = ""
        self.attrib = {}

    def Create(self,name,version,comments="",attrib=None):
        self.name = name
        self.version = version
        self.text = comments
        if attrib is not None: self.attrib = attrib

    def FromXML(self,element):
        if element.tag != "generator":
            raise Exception("Cannot construct LHAgenerator from: {0}".format(element.tag))
        for key, value in element.attrib.items():
            if key == "name": self.name = value
            elif key == "version": self.version = value
            else: self.attrib[key] = value
        self.text = element.text

    def ToXML(self,root):
        xml = ElementTree.SubElement(root,"generator")
        if self.name != "": xml.attrib["name"] = self.name
        if self.version != "": xml.attrib["version"] = self.version
        for key, value in self.attrib.items():
            xml.attrib[key] = str(value)
        xml.text = str(self.text)
        return xml

class LHAwgt:
    def __init__(self):
        self.id = ""
        self.text = 1.0
        self.attrib = {}

    def FromXML(self,element):
        if element.tag != "wgt":
            raise Exception("Cannot construct LHAwgt from: {0}".format(element.tag))
        for key, value in element.attrib.items():
            if key == "id": self.id = value
            else: self.attrib[key] = value
        self.text = element.text

    def ToXML(self,root):
        xml = ElementTree.SubElement(root,"wgt")
        for key, value in self.attrib.items():
            xml.attrib[key] = str(value)
        xml.text = str(self.text)
        return xml

class LHAweight:
    def __init__(self):
        self.id = ""
        self.text = ""
        self.attrib = {}

    def FromXML(self,element):
        if element.tag != "weight":
            raise Exception("Cannot construct LHAweight from: {0}".format(element.tag))
        self.id = element.attrib["id"]
        self.text = element.text
        self.attrib = {}

    def ToXML(self,root):
        self.xml = ElementTree.SubElement(root,"weight")
        self.xml.attrib["id"] = str(self.id)
        self.xml.text = str(self.text)
        for key, value in self.attrib.items():
            self.xml[key] = str(value)
        return self.xml

class LHAweightgroup:
    def __init__(self):
        self.name = ""
        self.text = ""
        self.weights = {}
        self.attrib = {}

    def AddWeight(self,weights):
        wt = LHAweight()
        wt.id = len(self.weights)+1
        for key, value in weights.items():
            if key == "id": wt.id = value
            else: wt.text += " " + str(key) + "=" + str(value) + " "
        self.weights[wt.id] = wt

    def AddWeights(self,weights):
        for weight in weights:
            self.AddWeight(weight)

    def FromXML(self,element):
        if element.tag != "weightgroup":
            raise Exception("Cannot construct LHAweightgroup from: {0}".format(element.tag))
        for key, value in element.attrib.items():
            if key == "name": self.name = value
            else: self.attrib[key] = value
        self.text = element.text

        # Now add the weights
        for child in element.findall("weight"):
            wt = LHAweight()
            wt.FromXML(child)
            self.weights[wt.id] = wt

    def ToXML(self,root):
        xml = ElementTree.SubElement(root,"weightgroup")
        if self.name != "": xml.attrib["name"] = str(self.name)
        for key, value in self.attrib.items():
            xml.attrib[key] = str(value)
        for key, value in self.weights.items():
            subtag = value.ToXML(xml)
        xml.text = str(self.text)
        return xml

class LHArwgt:
    def __init__(self):
        self.text = ""
        self.wgts = {}
        self.attrib = {}

    def FromXML(self,element):
        if element.tag != "rwgt":
            raise Exception("Cannot construct LHArwgt from: {0}".format(element.tag))
        for key, value in element.attrib.items():
            self.attrib[key] = value
        self.text = element.text

        # Now add the weights
        for child in element.findall("wgt"):
            wt = LHAwgt()
            wt.FromXML(child)
            self.wgts[wt.id] = wt

    def ToXML(self,root):
        xml = ElementTree.SubElement(root,"rwgt")
        for key, value in self.attrib.items():
            xml.attrib[key] = str(value)
        for key, value in self.wgts.items():
            subtag = value.ToXML(xml)
        xml.text = str(self.text)
        return xml

class LHAinitrwgt:
    def __init__(self):
        self.text = ""
        self.weights = {}
        self.weightgroups = {}
        self.attrib = {}

    def FromXML(self,element):
        if element.tag != "initrwgt":
            raise Exception("Cannot construct LHArwgt from: {0}".format(element.tag))
        for key, value in element.attrib.items():
            self.attrib[key] = value
        self.text = element.text

        # Now add the weights
        for child in element.findall("weightgroup"):
            wgroup = LHAweightgroup()
            wgroup.FromXML(child)
            self.weightgroups[wgroup.name] = wgroup

        for child in element.findall("weight"):
            wt = LHAweight()
            wt.FromXML(child)
            self.weights[wt.id] = wt

    def ToXML(self,root):
        xml = ElementTree.SubElement(root,"initrwgt")
        for key, value in self.attrib.items():
            xml.attrib[key] = str(value)
        for key, value in self.weightgroups.items():
            subtag = value.ToXML(xml)
        for key, value in self.weights.items():
            subtag = value.ToXML(xml)
        xml.text = str(self.text)
        return xml

class LHAinit:
    def __init__(self):
        self.text = ""
        self.attrib = {}
        self.generators = []
        self.beams = []
        self.energy = []
        self.pdfgroup = []
        self.pdfset = []
        self.wgtid = -4
        self.processes = []

    def FromXML(self,element):
        if element.tag != "init":
            raise Exception("Cannot construct LHAinit from: {0}".format(element.tag))
        for key, value in element.attrib.items():
            self.attrib[key] = value
        self.text = element.text

        # Now add the generators
        for child in element.findall("generator"):
            generator = LHAgenerator()
            generator.FromXML(child)
            self.generators.append(generator)

    def ToXML(self,root):
        xml = ElementTree.SubElement(root,"init")
        for key, value in self.attrib.items():
            xml.attrib[key] = str(value)
        for generator in self.generators:
            subtag = generator.ToXML(xml)
        xml.text = "\n {:>8} {:>8} ".format(*self.beams)
        xml.text += "{:>14} {:>14} ".format(*self.energy)
        xml.text += "{:>4} {:>4} ".format(*self.pdfgroup)
        xml.text += "{:>4} {:>4} ".format(*self.pdfset)
        xml.text += "{:>4} {:>4}\n".format(self.wgtid,len(self.processes))
        for process in self.processes:
            xml.text += str(process)
        xml.text += self.text
        return xml

class LHAProcess:
    def __init__(self,xsec,xerr,xmax,processid):
        self.xsec = xsec
        self.xerr = xerr
        self.xmax = xmax
        self.id = processid

    def __str__(self):
        return " {:>14} {:>14} {:>14} {:>6}\n".format(self.xsec,self.xerr,self.xmax,self.id)

    def __repr__(self):
        return " {:>14} {:>14} {:>14} {:>6}\n".format(self.xsec,self.xerr,self.xmax,self.id)

class LHAevent:
    def __init__(self):
        self.text = ""
        self.attrib = {}
        self.rwgt = None
        self.weights = None
        self.scales = None
        self.particles = []
        self.nprocess = -1
        self.wgt = 0
        self.scale = 0
        self.aqed = 0
        self.aqcd = 0

    def FromXML(self,element):
        for key, value in element.attrib.items():
            self.attrib[key] = value
        self.text = element.text

        # Add in rwgt information
        rwgt = element.find("rwgt")
        if rwgt is not None: 
            self.rwgt = LHArwgt()
            self.rwgt.FromXML(rwgt)

        # Add in weights information
        weights = element.find("weights")
        if weights is not None: 
            self.weights = LHAweights()
            self.weights.FromXML(weights)

        # Add in scales information
        scales = element.find("scales")
        if scales is not None: 
            self.scales = LHAscales()
            self.scales.FromXML(scales)

    def ToXML(self,root):
        xml = ElementTree.SubElement(root,"event")
        for key, value in self.attrib.items():
            xml.attrib[key] = value
        xml.text = "\n {:>4} {:>6} {:>14} {:>14} {:>14} {:>14}\n".format(
                len(self.particles),self.nprocess,self.wgt,
                self.scale,self.aqed,self.aqcd)
        for part in self.particles:
            xml.text += str(part)
        xml.text += str(self.text)
        xml.text += "    "
        if self.rwgt is not None: rwgttag = self.rwgt.ToXML(xml)
        if self.weights is not None: weightstag = self.weights.ToXML(xml)
        if self.scales is not None: scalestag = self.scales.ToXML(xml)
        return xml

class LHAParticle:
    def __init__(self,pid,status,momentum,col=[0,0],mothers=[0,0],vtim=0,spin=9):
        self.pid = pid
        self.status = status
        self.mom = momentum
        self.col = col
        self.mothers = mothers
        self.vtim = vtim
        self.spin = spin

    def __str__(self):
        string = " {:>8} {:>2} ".format(self.pid,self.status)
        string += "{:>4} {:>4} ".format(*self.mothers)
        string += "{:>4} {:>4} ".format(*self.col)
        string += "{:>18} {:>18} {:>18} {:>18} {:>18} ".format(*self.mom)
        string += "{:>1} {:>1}\n".format(self.vtim,self.spin)
        return string

    def __repr__(self):
        return str(self)

class LHAcustomTag:
    def __init__(self,tagname):
        self.tagname = tagname
        self.attrib = {}
        self.text = ""

    def FromXML(self,element):
        if element.tag != self.tagname:
            raise Exception("Cannont construct LHA{0} from: {1}".format(
                self.tagname,element.tag))
        for key, value in element.attrib.items():
            self.attrib[key] = value
        self.text = element.text

    def ToXML(self,root):
        xml = ElementTree.SubElement(root,self.tagname)
        for key, value in self.attrib.items():
            xml.attrib[key] = str(value)
        xml.text = self.text
        return xml

class LHEF:
    def __init__(self,filename):
        self.filename = filename
        self.events = []

    def Read(self):
        self.tree = ElementTree.parse(self.filename)
        self.root = self.tree.getroot()
        if self.root.tag != "LesHouchesEvents": 
            raise Exception("Invalid File Format: Incorrect header")
        try:
            self.version = self.root.attrib["version"]
        except KeyError:
            raise Exception("Invalid File Format: No version number")

        self.header = self.root.find("header")
        for child in self.header:
            if child.tag == "initrwgt":
                self.initrwgt = LHAinitrwgt()
                self.initrwgt.FromXML(child)

        self.init = LHAinit()
        self.init.FromXML(self.root.find("init"))

        self.events = []
        for eventXML in self.root.iter("event"):
            event = LHAevent()
            event.FromXML(eventXML)
            self.events.append(event)

    def SetHeader(self,version,initrwgt=None,headerComments=None):
        self.root = ElementTree.Element("LesHouchesEvents")
        self.root.attrib["version"] = str(version)
        self.header = ElementTree.SubElement(self.root,"header")
        if headerComments is not None:
            self.headerComments = headerComments
        if initrwgt is not None:
            self.initrwgt = initrwgt
            self.initrwgt.ToXML(self.header)

    def SetInitBlock(self,init):
        self.init = init
        self.init.ToXML(self.root)

    def AddEvent(self,event):
        self.events.append(event)
        self.events[-1].ToXML(self.root)

    def Write(self,outfile=None):
        xmlstr = minidom.parseString(ElementTree.tostring(self.root)).toprettyxml(indent="    ")
        if outfile is None:
            with open(self.filename,'w') as out:
                out.write(xmlstr[23:])
        else:
            with open(outfile,'w') as out:
                out.write(xmlstr[23:])

    # Written specifically for the LC and FC shower codes
    def CreateEvent(self,event,weight):
        lEvent = LHAevent()
        lEvent.nprocess = 9999
        lEvent.wgt = weight
        lEvent.scale = 91.2
        lEvent.aqed = 1.0/128.0
        lEvent.aqcd = 0.118
        lPart = LHAParticle(-event[0].pid,-1,Vec4M(-event[0].mom),col=[0,0])
        lEvent.particles.append(lPart)
        lPart = LHAParticle(-event[1].pid,-1,Vec4M(-event[1].mom),col=[0,0])
        lEvent.particles.append(lPart)
        for part in event[2:]:
            lPart = LHAParticle(part.pid,1,Vec4M(part.mom),part.col,mothers=[1,2])
            lEvent.particles.append(lPart)
        self.events.append(lEvent)

    def AddAllEvents(self):
        for event in self.events:
            event.ToXML(self.root)

def Vec4M(mom):
    mass2=mom.E*mom.E - mom.px*mom.px - mom.py*mom.py - mom.pz*mom.pz
    mass=0.
    if mass2>0.:
      mass = sqrt(mass2)
    if mass < 1e-4:
      mass = 0.0
    return [mom.px,mom.py,mom.pz,mom.E,mass]

class LHEFcontainer:

    def __init__(self,filename, ecm):
        # LHEF Initialization
        self.lhe = LHEF(filename)
        self.initrwgt = LHAinitrwgt()
        self.wt = LHAweight()
        self.wt.id = "central"
        self.wt.text = " mur=" + str(0.5*ecm) + " muf=" + str(0.5*ecm)
        self.initrwgt.weights[self.wt.id] = self.wt
        self.lhe.SetHeader(3.0,self.initrwgt)
        self.init = LHAinit()
        self.init.beams = [11,-11]
        self.init.energy = [0.5*ecm,0.5*ecm]
        self.init.pdfgroup = [-1,-1]
        self.init.pdfset = [-1,-1]
        self.init.wgtid = -4
        self.xsec = 0.
        self.xerr = 0.

    def update(self,wt,hardevent,lhevent):
        self.xsec += wt
        self.xerr += wt*wt
        event=copy.deepcopy(lhevent)
        event.insert(0,hardevent[1])
        event.insert(0,hardevent[0])
        self.colorize(event)
        self.lhe.CreateEvent(event,wt)

    def finalize(self):
        process = LHAProcess(self.xsec,self.xerr,self.xsec,9999)
        self.init.processes.append(process)
        self.lhe.SetInitBlock(self.init)
        self.lhe.AddAllEvents()
        self.lhe.Write()

    def colorize(self,event):
        # now construct a trivial color state.
        colindex=1
        for p in event:
          if p.pid > 0 and p.pid < 10:
            p.col = [colindex,0]
          if p.pid == 21:
            p.col = [colindex+1,colindex]
            colindex=colindex+1
          if p.pid < 0 and p.pid > -10:
            p.col = [0,colindex]
        return True

if __name__ == "__main__":
    test = LHEF("test_write.lhe")
    initrwgt = LHAinitrwgt()
    weightgroup = LHAweightgroup()
    weightgroup.attrib["type"] = "scale_variation"
    weightgroup.attrib["combine"] = "envelope"
    weights = [{"mur": 0.1E1, "muf": 0.1E1},
               {"mur": 0.1E1, "muf": 0.2E1},
               {"mur": 0.1E1, "muf": 0.5E0},
               {"mur": 0.2E1, "muf": 0.1E1},
               {"mur": 0.2E1, "muf": 0.2E1},
               {"mur": 0.2E1, "muf": 0.5E0},
               {"mur": 0.5E0, "muf": 0.1E1},
               {"mur": 0.5E0, "muf": 0.2E1},
               {"mur": 0.5E0, "muf": 0.5E0}]
    weightgroup.AddWeights(weights)

    initrwgt.weightgroups[weightgroup.name] = weightgroup
    test.SetHeader(3.0,initrwgt)

    init = LHAinit()
    init.beams = [2212,2212]
    init.energy = [4000,4000]
    init.pdfgroup = [-1,-1]
    init.pdfset = [21100,21100]
    init.wgtid = -4
    process = LHAProcess(0.501090862E2,0.89185414E-1,0.50109093E2,66)
    init.processes.append(process)
    gen = LHAgenerator()
    gen.Create("SomeGen1","1.2.3","Some additional comments")
    init.generators.append(gen)
    gen = LHAgenerator()
    gen.Create("SomeGen2","a.x.3","Some other comments")
    init.generators.append(gen)
    gen = LHAgenerator()
    gen.Create("SomeGen3","+.+.@","more comments")
    init.generators.append(gen)

    test.SetInitBlock(init)

    event = LHAevent()
    event.attrib["npLO"]=" -1 "
    event.attrib["npNLO"]=" 1 "
    event.nprocess = 66
    event.wgt = 0.5010903E2
    event.scale = 0.14137688E3
    event.aqed = 0.75563862E-2
    event.aqcd = 0.12114027E0

    part = LHAParticle(5,-1,[0,0,0.1437688E3,1433E3,0.48E1],col=[501,0],spin=0)
    event.particles.append(part)

    part = LHAParticle(2,-1,[0,0,0.1437688E3,1433E3,0.48E1],col=[501,0],spin=0)
    event.particles.append(part)

    part = LHAParticle(24,1,[0,0,0.1437688E3,1433E3,0.48E1],col=[501,0],spin=0)
    event.particles.append(part)

    part = LHAParticle(5,1,[0,0,0.1437688E3,1433E3,0.48E1],col=[501,0],spin=0)
    event.particles.append(part)

    part = LHAParticle(2,1,[0,0,0.1437688E3,1433E3,0.48E1],col=[501,0],spin=0)
    event.particles.append(part)

    test.AddEvent(event)

    test.Write()
