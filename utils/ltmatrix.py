
from vector import Vec4
from math import sqrt, cos, sin

import jax.numpy as jnp
from jax import config
config.update("jax_enable_x64", True)


class LTmatrix:

    def __init__(self):
        self.M = [[1.,0.,0.,0.], [0.,1.,0.,0.], [0.,0.,1.,0.], [0.,0.,0.,1.]]

    def __mul__(self,v):

        x = v.px
        y = v.py
        z = v.pz
        t = v.E;
        tt = self.M[0][0] * t + self.M[0][1] * x + self.M[0][2] * y +  self.M[0][3] * z;
        xx = self.M[1][0] * t + self.M[1][1] * x + self.M[1][2] * y +  self.M[1][3] * z;
        yy = self.M[2][0] * t + self.M[2][1] * x + self.M[2][2] * y +  self.M[2][3] * z;
        zz = self.M[3][0] * t + self.M[3][1] * x + self.M[3][2] * y +  self.M[3][3] * z;

        return Vec4(tt,xx,yy,zz)

    def printMatrix(self):
        print ("|", self.M[0][0] , self.M[0][1] , self.M[0][2] ,  self.M[0][3] , "|")
        print ("|", self.M[1][0] , self.M[1][1] , self.M[1][2] ,  self.M[1][3] , "|")
        print ("|", self.M[2][0] , self.M[2][1] , self.M[2][2] ,  self.M[2][3] , "|")
        print ("|", self.M[3][0] , self.M[3][1] , self.M[3][2] ,  self.M[3][3] , "|")


    def FromCMframe(self,p1, p2):
        pSum = p1 + p2

        #print "DEBUG!!! 0 ", pSum.M()

        d  = p1
        d.bstback(pSum)

        #psum1 = Vec4(pSum.E, pSum.px, pSum.py, pSum.pz)
        #print "DEBUG!!! a ", pSum, psum1
        #psum1.bstback(pSum)

        theta = d.Theta()
        phi   = d.Phi()
        self.rot(0., -phi)

        #print "DEBUG!!! b ", pSum, psum1, theta, phi
        #self.printMatrix()

        self.rot(theta, phi)

        #print "DEBUG!!!"
        #self.printMatrix()

        self.bst(pSum)

        #print "DEBUG!!!"
        #self.printMatrix()

    def bstback(self,p):
        betaX = -p.px / p.E
        betaY = -p.py / p.E
        betaZ = -p.pz / p.E

        #print "DEBUG!! in bstback beta ", betaX, betaY, betaZ, betaX**2 + betaY**2 + betaZ**2

        #self.bst(betaX, betaY, betaZ)
        self.boost(betaX, betaY, betaZ)

    def bst(self,p):
        betaX = p.px / p.E
        betaY = p.py / p.E
        betaZ = p.pz / p.E

        #print "DEBUG!! in bst beta ", betaX, betaY, betaZ, betaX**2 + betaY**2 + betaZ**2

        self.boost(betaX, betaY, betaZ);

    def bst_p1_to_p2(self, p1, p2):
        eSum  = p1.E + p2.E
        betaX = (p2.px - p1.px) / eSum
        betaY = (p2.py - p1.py) / eSum
        betaZ = (p2.pz - p1.pz) / eSum
        fac = 2. / (1. + betaX*betaX + betaY*betaY + betaZ*betaZ)
        betaX *= fac
        betaY *= fac
        betaZ *= fac
        self.boost(betaX, betaY, betaZ)

    def rot(self,theta, phi):

        cthe = jnp.cos(theta)
        sthe = jnp.sin(theta)
        cphi = jnp.cos(phi)
        sphi = jnp.sin(phi)
        Mrot =  [
          [1.,           0.,         0.,          0.],
          [0.,  cthe * cphi,     - sphi, sthe * cphi],
          [0.,  cthe * sphi,       cphi, sthe * sphi],
          [0., -sthe,                0., cthe       ]]

        #Mtmp = self.M
        #self.M = Mrot.dot(Mtmp)

        Mtmp =  [[1.,0.,0.,0.], [0.,1.,0.,0.], [0.,0.,1.,0.], [0.,0.,0.,1.]]
        for i in range(0,4):
          for j in range(0,4):
            Mtmp[i][j] = self.M[i][j]

        for i in range(0,4):
          for j in range(0,4):
            self.M[i][j] = Mrot[i][0] * Mtmp[0][j] + Mrot[i][1] * Mtmp[1][j] + Mrot[i][2] * Mtmp[2][j] + Mrot[i][3] * Mtmp[3][j];

    def boost(self,betaX, betaY, betaZ):

        gm = 1. / jnp.sqrt( max( 1e-10, 1. - betaX*betaX - betaY*betaY
                                       - betaZ*betaZ ) )

        #print "DEBUG!! gamma ", gm 
        gf = gm*gm / (1. + gm)
        Mbst = [
          [ gm,           gm*betaX,           gm*betaY,          gm*betaZ ],
          [ gm*betaX, 1. + gf*betaX*betaX, gf*betaX*betaY, gf*betaX*betaZ ],
          [ gm*betaY, gf*betaY*betaX, 1. + gf*betaY*betaY, gf*betaY*betaZ ],
          [ gm*betaZ, gf*betaZ*betaX, gf*betaZ*betaY, 1. + gf*betaZ*betaZ ]]

        #Mtmp = self.M
        #self.M = Mbst.dot(Mtmp)

        Mtmp =  [[1.,0.,0.,0.], [0.,1.,0.,0.], [0.,0.,1.,0.], [0.,0.,0.,1.]]
        for i in range(0,4):
          for j in range(0,4):
            Mtmp[i][j] = self.M[i][j]

        for i in range(0,4):
          for j in range(0,4):
            self.M[i][j] = Mbst[i][0] * Mtmp[0][j] + Mbst[i][1] * Mtmp[1][j] + Mbst[i][2] * Mtmp[2][j] + Mbst[i][3] * Mtmp[3][j];



