
#import math as m
#import random as r
#from math import sqrt, cos, sin
#from math import log as ln

import jax.numpy as jnp
from jax import config
config.update("jax_enable_x64", True)

import sys

from vector import Vec4
from ltmatrix import LTmatrix
from particle import Particle
#from thrust import Thrust

from basics import eq, ne, le, lt, gt, ge

class Kinematics:

    #def __init__(self, primitive, randomState):
    def __init__(self, primitive, verbosity=0):
        self.primitive = primitive
        self.usedTips = []
        self.verbosity=verbosity
        #self.randomState = randomState
        return None

###############################################################################
    def createEvolvedEvent(self,event, ecm, k_lambda, LambdaQCD, phi0, phi1, phi2, phi3, phi4, phi5):

        self.usedTips = []

        #kappaMax = jnp.log(ecm*ecm/(LambdaQCD*LambdaQCD))
        kappaMax = 100.
        foundBranching = True
        counter=0
        while foundBranching and counter < 1000:
          counter=counter+1
          foundBranching, splitIndex = self.getNextSplitting(kappaMax)
          if foundBranching == False: break
          for i in range(len(splitIndex)):
            kappaMax = self.primitive.split_systems[i][3]
            self.updateKinematics(splitIndex[i],event, k_lambda, LambdaQCD, phi0, phi1, phi2, phi3, phi4, phi5)
            self.usedTips.append(splitIndex[i])
        self.updateEvent(event)

        if not self.testMomentumConservation(event):
          print ("what?")
          sys.exit()
          return False

        return True

    def testMomentumConservation(self, event):

        pIn = event[0].mom + event[1].mom
        pOut = Vec4(0.,0.,0.,0.)
        for i in range(2,len(event)):
          pOut = pOut + event[i].mom

        if  ne(pIn.E, pOut.E) or ne(pIn.px,pOut.px) or ne(pIn.py,pOut.py) or ne(pIn.pz,pOut.pz):
          print ("Error: Momentum not conserved:", self.primitive.name)
          print (pIn)
          print (pOut)
          for p in event:
            print (p)
          #sys.exit()
          return False

        return True


###############################################################################
    def getEvolvedEvent(self,event, ecm, LambdaQCD):

        if self.createEvolvedEvent(event, ecm, LambdaQCD):
          return event

        print ("error, could not create evolved event")
        return None

###############################################################################
    def findNeighbors(self,event, iSplit):

        posMin=10000000
        posMax=-1
        for s in self.primitive.split_systems:
          posMin = min(posMin, s[0])
          posMax = max(posMax, s[2])

        iPosEventIndex=0
        iFind=iSplit+1
        found=False
        while not found and iFind <= posMax:
          for ip in range(len(event)):
            if event[ip].pos == iFind and event[ip].pos != 1:
              found=True
              iPosEventIndex=ip
              break
          if not found:
            iFind=iFind+1

        iNegEventIndex=0
        iFind=iSplit-1
        found=False
        while not found and iFind >= posMin:
          for ip in range(len(event)):
            if event[ip].pos == iFind and event[ip].pos != 2:
              found=True
              iNegEventIndex=ip
              break
          if not found:
            iFind=iFind-1

        if iPosEventIndex==0:
          for ip in range(len(event)):
            if event[ip].pos == 2:
              iPosEventIndex=ip
              break

        if iNegEventIndex==0:
          for ip in range(len(event)):
            if event[ip].pos == 1:
              found=True
              iNegEventIndex=ip
              break

        if iPosEventIndex==0 or iNegEventIndex==0:
          print ("could not find iPos or iNeg", posMin, iSplit, posMax)
          for p in event:
            print (p.pos, p)
          sys.exit()

        return iNegEventIndex, iPosEventIndex 

###############################################################################

    def updateKinematics(self,splitIndex,event, k_lambda, LambdaQCD, phi0, phi1, phi2, phi3, phi4, phi5):

        #if self.verbosity>0:
        #  print ("enter updateKinematics ", len(event), len(self.primitive.split_systems), self.primitive.split_systems[splitIndex][1])
        #  for s in range(len(self.primitive.split_systems)):
        #    print (self.primitive.split_systems[s])

        split = self.primitive.split_systems[splitIndex]
        iSplit = split[1]
        ipNeg, ipPos = self.findNeighbors(event,iSplit)
        iNeg = event[ipNeg].pos
        iPos = event[ipPos].pos

        pi_old = copy.deepcopy(event[ipNeg].mom)
        pk_old = copy.deepcopy(event[ipPos].mom)

        if  le ((event[ipNeg].mom+event[ipPos].mom).M2(), 0.):
          print ("Error in kinematics: sijk =", (event[ipNeg].mom+event[ipPos].mom).M2(), " < 0.0")
          return False

        lambda13 = self.primitive.generalized_rapidity(iNeg,iSplit)
        lambda23 = self.primitive.generalized_rapidity(iSplit,iPos)
        lambda12 = self.primitive.generalized_rapidity(iNeg,iPos)

        if le(lambda13,0.) or le(lambda23,0.):
          print ("Error in kinematics: Zero lambda measures ")
          sys.exit()
          return False 

        sij=k_lambda*k_lambda*LambdaQCD*LambdaQCD*jnp.exp(lambda13)
        sjk=k_lambda*k_lambda*LambdaQCD*LambdaQCD*jnp.exp(lambda23)
        #sijk = k_lambda*k_lambda*LambdaQCD*LambdaQCD*jnp.exp(lambda12)
        sijk = ((event[ipNeg].mom+event[ipPos].mom).M2())

        #sij=1682.04495007
        #sjk=4085.09961484
        #sijk=8317.44




        #print ("k_lambda")
        #print (k_lambda)
        #print ("sij=")
        #print (sij)
        #print ("sjk=")
        #print (sjk)
        #print ("sijk=")
        #print (sijk)

        if sij + sjk > sijk:
          sijk_lambda = k_lambda*k_lambda*LambdaQCD*LambdaQCD*jnp.exp(lambda12)

          if self.verbosity>0:
            print ("Error in kinematics for event size ", len(event), " and primitive", self.primitive.name, ": sij+sjk=",sij+sjk, "> sijk=",sijk, sijk_lambda)

            deltaYg=11./6.

            sys=self.primitive.system_of_particle(iNeg)
            ktneg=-1.0
            if sys>-1:
              ktneg=self.primitive.split_systems[sys][3]
            sys=self.primitive.system_of_particle(iSplit)
            ktsplit=-1.0
            if sys>-1:
              ktsplit=self.primitive.split_systems[sys][3]
            sys=self.primitive.system_of_particle(iPos)
            ktpos=-1.0
            if sys>-1:
              ktpos=self.primitive.split_systems[sys][3]


            print ("iNeg=", iNeg, ktneg, " iSplit=", iSplit, ktsplit, " iPos=", iPos, ktpos, "   ", 2*deltaYg, 4.*deltaYg )
            print ("l13=",lambda13,"l23=",lambda23,"l12=",lambda12 )

          sij *= sijk/sijk_lambda
          sjk *= sijk/sijk_lambda
          #return False

        mI=0.
        mK=0.
        mi=0.
        mj=0.
        mk=0.
        #phi = 2.*jnp.pi*self.randomState.get_next_rphi(iSplit-3)
        phi = 2.*jnp.pi
        if iSplit-3==0:
          phi*=phi0
        if iSplit-3==1:
          phi*=phi1
        if iSplit-3==2:
          phi*=phi2
        if iSplit-3==3:
          phi*=phi3
        if iSplit-3==4:
          phi*=phi4
        if iSplit-3==5:
          phi*=phi5

        #success, postBranchingMomenta = self.branch(event,k_lambda*LambdaQCD, mI,mK,mi,mj,mk,sijk,sij-mi*mi-mj*mj,sjk-mj*mj-mk*mk, phi)
        success, postBranchingMomenta = self.branch(mI,mK,mi,mj,mk,sijk,sij-mi*mi-mj*mj,sjk-mj*mj-mk*mk, phi)

        if not success: return False

        fromCM = LTmatrix()
        fromCM.FromCMframe(event[ipNeg].mom,event[ipPos].mom)

        #print ("ami2=")
        #print (postBranchingMomenta[0].M2())
        #print ("amj2=")
        #print (postBranchingMomenta[1].M2())
        #print ("amk2=")
        #print (postBranchingMomenta[2].M2())

        pi = fromCM*postBranchingMomenta[0]
        pj = fromCM*postBranchingMomenta[1]
        pk = fromCM*postBranchingMomenta[2]

        #print ("bmi2=")
        #print (str(pi))
        #print (pi.M2())
        #print ("bmj2=")
        #print (str(pj))
        #print (pj.M2())
        #print ("bmk2=")
        #print (str(pk))
        #print (pk.M2())

        event.append(Particle(21,pj,[0,0]))
        event[-1].SetPos(split[1])
        event[ipNeg].mom = pi
        event[ipPos].mom = pk

        if not self.testMomentumConservation(event):
          event.pop()
          event[ipNeg].mom = pi_old
          event[ipPos].mom = pk_old
          print ("fail momentum check, dicard emission")
          return False

        #event.append(Particle(222,postBranchingMomenta[0],[0,0]))
        #event.append(Particle(222,postBranchingMomenta[1],[0,0]))
        #event.append(Particle(222,postBranchingMomenta[2],[0,0]))
        #t = Thrust()
        #tnow = t.calc_on_event(event)
        #if lt(tnow, 0.83) and eq(k_lambda*LambdaQCD,0.2):
        #  if self.verbosity>0:
        #    print ("low thrust tnow=",tnow, sij, sjk, sijk, sijk-sij-sjk, phi)
            
        return True

    #def branch(self,event,lam,mI,mK,mi,mj,mk,sijk,sij,sjk,phi):
    def branch(self,mI,mK,mi,mj,mk,sijk,sij,sjk,phi):

       #print ("branch sij=")
       #print (sij)

       #print ("branch sjk=")
       #print (sjk)


       # Convert to names used in Vincia
       m2I=mI*mI
       m2K=mK*mK
       m2Ant=sijk
       mAnt=jnp.sqrt(m2Ant)
       sAnt = m2Ant - m2I - m2K;

       mass0 = mi
       mass1 = mj
       mass2 = mk

       s01 = sij
       s12 = sjk
       s02 = m2Ant - s01 - s12 - mass0*mass0 - mass1*mass1 - mass2*mass2;

       E0 = (mass0*mass0 + s01/2 + s02/2)/mAnt;
       E1 = (mass1*mass1 + s12/2 + s01/2)/mAnt;
       E2 = (mass2*mass2 + s02/2 + s12/2)/mAnt;

       #print ("math ", sqrt( E0*E0 - mass0*mass0 ), "jnp sqrt", jnp.sqrt( E0*E0 - mass0*mass0 ), " jlax pow", jlax.pow( E0*E0 - mass0*mass0 , 0.5))

       #print ("E0=",E0)
       #print ("E1=",E1)
       #print ("E2=",E2)


       ap0 = jnp.sqrt( E0*E0 - mass0*mass0 );
       ap1 = jnp.sqrt( E1*E1 - mass1*mass1 );
       ap2 = jnp.sqrt( E2*E2 - mass2*mass2 );

       cos01 = (E0*E1 - s01/2)/(ap0*ap1);
       cos02 = (E0*E2 - s02/2)/(ap0*ap2);

       if ge(cos01,1.):
         cos01=1.0
       elif le(cos01,-1.):
         cos01=-1.0

       if ge(cos02,1.):
         cos02=1.0
       elif le(cos02,-1.):
         cos02=-1.0

       sin01 = jnp.sqrt(max(0.,1.-cos01*cos01))
       sin02 = jnp.sqrt(max(0.,1.-cos02*cos02))

       p1cm = Vec4(E0,0.0,0.0,ap0)
       p2cm = Vec4(E1,-ap1*sin01,0.0,ap1*cos01);
       p3cm = Vec4(E2,ap2*sin02,0.0,ap2*cos02);

       sig2 = m2Ant + m2I - m2K;
       sAntMin = 2*jnp.sqrt(m2I*m2K);
       s01min  = 2*mass0*mass1;
       s12min  = 2*mass1*mass2;
       s02min  = 2*mass0*mass2;

       gDet = ((s01*s12*s02 - s01*s01*mass2*mass2 - s02*s02*mass1*mass1 - s12*s12*mass0*mass0)/4 + mass0*mass0*mass1*mass1*mass2*mass2);

       if gDet < 0.:
         print ("small gram determinant, discard emission")
         return False, [Vec4(),Vec4(),Vec4()]
         #sys.exit()

       # The r and R parameters in arXiv:1108.6172.
       rAntMap = ( sig2 + jnp.sqrt( sAnt*sAnt - sAntMin*sAntMin ) * ( s12-s12min - (s01-s01min) ) / ( s01-s01min + s12-s12min ) ) / (2*m2Ant);
       bigRantMap2 = 16*gDet * ( m2Ant*rAntMap * (1.-rAntMap) - (1.-rAntMap)*m2I - rAntMap*m2K ) + ( s02*s02 - s02min*s02min ) * ( sAnt*sAnt - sAntMin*sAntMin );

       bigRantMap = jnp.sqrt( bigRantMap2 );
       p1dotpI = (sig2*(s02*s02 - s02min*s02min) * (m2Ant + mass0*mass0 - mass1*mass1 - mass2*mass2 - s12) + 8*rAntMap*(m2Ant + mass0*mass0 - mass1*mass1 - mass2*mass2 - s12)*gDet - bigRantMap*(s02*s02 - s02min*s02min + s01*s02-2*s12*mass0*mass0)) / (4*(4*gDet + m2Ant*(s02*s02 - s02min*s02min)));

       apInum2 = m2Ant*m2Ant + m2I*m2I + m2K*m2K - 2*m2Ant*m2I - 2*m2Ant*m2K - 2*m2I*m2K;

       apI = jnp.sqrt(apInum2)/(2*mAnt)
       EI = jnp.sqrt( apI*apI + m2I )
       cosPsi = ((E0*EI) - p1dotpI)/(ap0*apI)

       if jnp.greater_equal(cosPsi,1.):
         cosPsi=1.0
       elif jnp.less_equal(cosPsi,-1.):
         cosPsi=-1.0

       #cosPsi =  1.

       sinPsi = jnp.sqrt(1. - cosPsi*cosPsi)

       #phi = 2.*m.pi*r.random()
       cosPhi=jnp.cos(phi)

       #cosPhi=1.

       sinPhi=jnp.sqrt(1.-cosPhi*cosPhi)

       #pi = Vec4(Ei,0.,0.,Ei)
       p1 = Vec4(p1cm.E,
         cosPsi * cosPhi * p1cm.px -  sinPhi * p1cm.py + sinPsi * cosPhi * p1cm.pz,
         cosPsi * sinPhi * p1cm.px +  cosPhi * p1cm.py + sinPsi * sinPhi * p1cm.pz,
         -sinPsi*          p1cm.px +  0.               + cosPsi          * p1cm.pz)

       #print ("p1=")
       #print (str(p1))
       #print ("m21=",p1.M2())

       #pk = Vec4(Ek, Ek*sin(Thetaik), 0., Ek*cosThetaik)
       p3 = Vec4(p3cm.E,
         cosPsi * cosPhi * p3cm.px -  sinPhi * p3cm.py + sinPsi * cosPhi * p3cm.pz,
         cosPsi * sinPhi * p3cm.px +  cosPhi * p3cm.py + sinPsi * sinPhi * p3cm.pz,
         -sinPsi         * p3cm.px +  0.               + cosPsi          * p3cm.pz)

       #print ("p3=")
       #print (str(p3))
       #print ("m23=", p3.M2())

       #pj = Vec4(Ej, -Ej*sin(Thetaij), 0., Ej*cosThetaij)
       p2 = Vec4(p2cm.E,
         cosPsi * cosPhi * p2cm.px -  sinPhi * p2cm.py + sinPsi * cosPhi * p2cm.pz,
         cosPsi * sinPhi * p2cm.px +  cosPhi * p2cm.py + sinPsi * sinPhi * p2cm.pz,
         -sinPsi         * p2cm.px +  0.               + cosPsi          * p2cm.pz)

       #print ("p2=")
       #print (str(p2))
       #print ("m22=",p2.M2())
       #testevt=[]
       #testevt.append(event[0])
       #testevt.append(event[1])
       #testevt.append(Particle(21,p1,[0,0]))
       #testevt.append(Particle(21,p2,[0,0]))
       #testevt.append(Particle(21,p3,[0,0]))
       #ttt = Thrust()
       #tnow = ttt.calc_on_event(testevt)
       ##if lt(tnow, 1.0) and gt(tnow,0.):
       #if lt(tnow, 0.83) and gt(tnow,0.) and eq(lam,0.2):
       #  if self.verbosity>0:
       #    print ("low thrust tnow=",tnow, sij, sjk, "cosPhi=", cosPhi, "cosPsi=",cosPsi, "cos01=",cos01,"cos02=",cos02,"E0=", E0, "E1=", E1, "E2=", E2, "ap1=",ap1, "ap2=",ap2)
            
       return True, [p1,p2,p3]

    def getNextSplitting(self,kappaMax):

        #print ("enter getNextSplitting")

        kappaMaxNow=-1e10
        sMax= -1

        for s in range(len(self.primitive.split_systems)):
          split=self.primitive.split_systems[s]
          if s in self.usedTips:
            continue
          kappaNow=split[3]
          if kappaNow>kappaMaxNow and kappaNow < kappaMax:
            kappaMaxNow=kappaNow
            sMax = s

        if ge(sMax,0.):
          sMaxList = []
          for s in range(len(self.primitive.split_systems)):
            if s in self.usedTips:
              continue
            split=self.primitive.split_systems[s]
            kappaNow=split[3]
            if eq (kappaNow, kappaMaxNow):
              sMaxList.append(s)
          return True, sMaxList

        return False, []

    def updateEvent(self,event):

        # always put antiquark in last place in event
        for i in range(2,len(event)):
          for j in range(2,len(event)):
            if (event[j].pid < 0
               and event[j].pid > -10
               and event[i].pid > 0
               and (event[i].pid < 10 or event[i].pid == 21)):
                event[i], event[j] = event[j], event[i]

        # now construct a trivial color state.
        colindex=1
        for p in event:
          if p.pid > 0 and p.pid < 10:
            p.col = [colindex,0]
          if p.pid == 21:
            p.col = [colindex+1,colindex]
            colindex=colindex+1
          if p.pid < 0 and p.pid > -10:
            p.col = [0,colindex]

        return True

import copy
from jax import jacfwd, jacrev
from basics import convertEventToArray
from primitive import Primitive

def getEvolvedEvent(hardevent, structName, ecm, k_lambda, LambdaQCD, verbosity, ry0,ry1,ry2,ry3,ry4,ry5,rk0,rk1,rk2,rk3,rk4,rk5,  phi0, phi1, phi2, phi3, phi4, phi5):
    primitive = Primitive(structName, ecm, k_lambda, LambdaQCD)
    #primitive.randomState = randState
    primitive.calc_kappa_min_secondary()
    primitive.smear_particles(ry0,ry1,ry2,ry3,ry4,ry5,rk0,rk1,rk2,rk3,rk4,rk5)
    #kinematics = Kinematics(primitive, randState)
    kinematics = Kinematics(primitive, verbosity)
    event=copy.deepcopy(hardevent)
    #weight = primitive.event_weight()
    if kinematics.createEvolvedEvent(event, ecm, k_lambda, LambdaQCD, phi0, phi1, phi2, phi3, phi4, phi5):
      event.append(Particle(0,Vec4(primitive.event_weight(),0.,0.,0.),[0,0]))
      ret = convertEventToArray(event,2)
      return ret
    print ("error, could not create evolved event")
    return None

#jac_event = jacrev(getEvolvedEvent, argnums=3)
#jac_event = jacfwd(getEvolvedEvent, argnums=3)
d1_evtgen = jacfwd(getEvolvedEvent, argnums=3)
d2_evtgen = jacfwd(jacrev(getEvolvedEvent,argnums=3), argnums=3)
d3_evtgen = jacrev(jacfwd(jacrev(getEvolvedEvent,argnums=3), argnums=3), argnums=3)
d4_evtgen = jacfwd(jacrev(jacfwd(jacrev(getEvolvedEvent,argnums=3), argnums=3), argnums=3), argnums=3)
d5_evtgen = jacrev(jacfwd(jacrev(jacfwd(jacrev(getEvolvedEvent,argnums=3), argnums=3), argnums=3), argnums=3), argnums=3)
d6_evtgen = jacfwd(jacrev(jacfwd(jacrev(jacfwd(jacrev(getEvolvedEvent,argnums=3), argnums=3), argnums=3), argnums=3), argnums=3), argnums=3)

def getEvolvedKinematics(hardevent, primitive, ecm, k_lambda, LambdaQCD, phi0, phi1, phi2, phi3, phi4, phi5):
    kinematics = Kinematics(primitive)
    event=copy.deepcopy(hardevent)
    if kinematics.createEvolvedEvent(event, ecm, k_lambda, LambdaQCD, phi0, phi1, phi2, phi3, phi4, phi5):
      event.append(Particle(0,Vec4(primitive.event_weight(),0.,0.,0.),[0,0]))
      ret = convertEventToArray(event,2)
      #for r in ret:
      #  print ("particle", r)
      return ret
    print ("error, could not create evolved event")
    return None

d1_kinematics = jacfwd(getEvolvedKinematics, argnums=3)
d2_kinematics = jacfwd(jacrev(getEvolvedKinematics,argnums=3), argnums=3)
d3_kinematics = jacrev(jacfwd(jacrev(getEvolvedKinematics,argnums=3), argnums=3), argnums=3)
d4_kinematics = jacfwd(jacrev(jacfwd(jacrev(getEvolvedKinematics,argnums=3), argnums=3), argnums=3), argnums=3)
d5_kinematics = jacrev(jacfwd(jacrev(jacfwd(jacrev(getEvolvedKinematics,argnums=3), argnums=3), argnums=3), argnums=3), argnums=3)
d6_kinematics = jacfwd(jacrev(jacfwd(jacrev(jacfwd(jacrev(getEvolvedKinematics,argnums=3), argnums=3), argnums=3), argnums=3), argnums=3), argnums=3)


