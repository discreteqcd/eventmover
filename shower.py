
# system imports
from __future__ import print_function
import sys, argparse
import numpy as np
import math as m

# Specialized imports
sys.path.append('utils')
from basics import convertArrayToEvent, discrete_inverse_trans
# hard ME imports
from matrix import eetojj
from LHEF3 import LHEFcontainer
# basic shower data structures
from primitive import Primitive
from primitive_kinematics import Kinematics
from primitive_kinematics import getEvolvedEvent,  d1_evtgen, d2_evtgen, d3_evtgen, d4_evtgen, d5_evtgen, d6_evtgen
from primitive_probabilities import getProbability
from particle import Particle
from vector import Vec4
from ltmatrix import LTmatrix
from basics import gt, lt, ne, is_onshell, is_physical, reshuffle, max_offshellness

LambdaQCD=0.4

# Get command line arguments
parser = argparse.ArgumentParser(description='This is a differentiable parton shower.')
parser.add_argument('--nevts', type=int, default=1000)
parser.add_argument('--order', type=int, default=2)
parser.add_argument('--seed', type=int, default=-1)
parser.add_argument('--ecm', type=float, default=91.2)
parser.add_argument('--lhefname', type=str, default="showered")
parser.add_argument('--print_control_lhefs', action='store_true')
parser.add_argument('--no-print_control_lhefs', dest='print_control_lhefs', action='store_false')
parser.set_defaults(print_control_lhefs=False)
parser.add_argument('--rescue', action='store_true')
parser.add_argument('--no-rescue', dest='rescue', action='store_false')
parser.set_defaults(rescue=True)

args = parser.parse_args()

# desired events and center-of-mass energy
nevents=args.nevts
ecm=args.ecm
seed=args.seed
order=args.order
lhefname=args.lhefname
print_c=args.print_control_lhefs
rescue=args.rescue

print ("\n*----------------------------------------------------------------------------*\n")
print ("          Welcome to EventMover -- a differentiable parton shower.\n")
print ("  Running shower at eCM=", ecm, " nevents=",nevents, " seed=", seed, " expansion order=", order, ". Store output to '", lhefname, "'")
if print_c:
  print ("  Writing control events.")
else:
  print ("  Do not write control events.")
if rescue:
  print ("  Rescue shifted events if necessary.")
else:
  print ("  Do not rescue shifted events.")

# create external random state
# random numbers for picking primitive structures
if seed > 0:
  np.random.seed(seed)
pick_primitive_vec =  np.random.uniform(0,1,nevents)
# random numbers for smearing kinematics
pick_smear_kappa = []
pick_smear_y = []
pick_phi = []
for i in range(6):
  if seed > 0:
    np.random.seed(seed+i*100+1)
  pick_smear_kappa.append(np.random.uniform(0,1,nevents))
  if seed > 0:
    np.random.seed(seed+i*100+2)
  pick_smear_y.append(np.random.uniform(0,1,nevents))
  if seed > 0:
    np.random.seed(seed+i*100+3)
  pick_phi.append(np.random.uniform(0,1,nevents))

if seed > 0:
  np.random.seed(seed+10000+1)
me_phi =  np.random.uniform(0,1,nevents)

if seed > 0:
  np.random.seed(seed+10000+2)
me_fl =  np.random.uniform(0,1,nevents)

if seed > 0:
  np.random.seed(seed+10000+3)
me_ct =  np.random.uniform(0,1,nevents)

# init event generation
hardprocess = eetojj(ecm)

probVec = []
names = []
for c in list(map(chr,range(ord('A'),ord('X')+1))):
  probVec.append(getProbability(c))
  names.append(c)

lhe = []
shifted_lhe = []
shift=[-0.5,-0.375,-0.25,0.25,0.375,0.5]
for s in shift:
  name="p"
  if s<0.0:
    name="n"
  shifted_lhe.append(LHEFcontainer(lhefname+"_shifted_" + str(order) + "_" + name + str(int(round(abs(s),3)*1000))+".lhe",ecm))
  if print_c:
    lhe.append(LHEFcontainer(lhefname+"_control_" + name +str(int(round(abs(s),3)*1000))+".lhe",ecm))

lhe.append(LHEFcontainer(lhefname+"_baseline.lhe",ecm))
k_lambda=1.0

# Event loop
nprint=1
for iev in range(0,nevents):

  if iev%nprint==0:
    print ("\n*------ event: #", iev,"-------------*")

  index = discrete_inverse_trans(probVec, pick_primitive_vec[iev])

  name = names[index]
  #print ("pick primitive", name)
  hardevent, weight = hardprocess.GeneratePoint(me_ct[iev],me_phi[iev],me_fl[iev])

  if hardevent[2].mom.Y() < 0.:
    hardevent[2].SetPos(1)
    hardevent[3].SetPos(2)
  else:
    hardevent[2].SetPos(2)
    hardevent[3].SetPos(1)

  evolvedEvent   = getEvolvedEvent(hardevent, name, ecm, k_lambda, LambdaQCD, 1,
    pick_smear_y[0][iev] ,pick_smear_y[1][iev] ,pick_smear_y[2][iev] ,pick_smear_y[3][iev] ,pick_smear_y[4][iev] ,pick_smear_y[5][iev] ,
    pick_smear_kappa[0][iev] ,pick_smear_kappa[1][iev] ,pick_smear_kappa[2][iev] ,pick_smear_kappa[3][iev] ,pick_smear_kappa[4][iev] ,pick_smear_kappa[5][iev] ,
    pick_phi[0][iev], pick_phi[1][iev], pick_phi[2][iev], pick_phi[3][iev], pick_phi[4][iev], pick_phi[5][iev])

  wgt = weight*evolvedEvent[-1][1]
  lhe[-1].update(wgt,hardevent,convertArrayToEvent(evolvedEvent))

  dE_dlambda   = d1_evtgen(hardevent, name, ecm, k_lambda, LambdaQCD, -1,
    pick_smear_y[0][iev] ,pick_smear_y[1][iev] ,pick_smear_y[2][iev] ,pick_smear_y[3][iev] ,pick_smear_y[4][iev] ,pick_smear_y[5][iev] ,
    pick_smear_kappa[0][iev] ,pick_smear_kappa[1][iev] ,pick_smear_kappa[2][iev] ,pick_smear_kappa[3][iev] ,pick_smear_kappa[4][iev] ,pick_smear_kappa[5][iev] ,
    pick_phi[0][iev], pick_phi[1][iev], pick_phi[2][iev], pick_phi[3][iev], pick_phi[4][iev], pick_phi[5][iev])

  for i in range(len(shift)):
    dlambda = shift[i] 

    shifted_event = evolvedEvent
    shifted_event += dlambda*dE_dlambda

    found_shift=False
    min_offshell_event = evolvedEvent

    max_mass=0.
    max_mass=max_offshellness(shifted_event)
    if is_physical(shifted_event,ecm):
      found_shift=True
      min_offshell_event = shifted_event

    idd=1
    while not is_onshell(shifted_event):
      idd+=1
      if idd==2 and idd<=order:
        shifted_event += 1./2.*m.pow(dlambda,2.) * d2_evtgen(hardevent, name, ecm, k_lambda, LambdaQCD, -1,
          pick_smear_y[0][iev],pick_smear_y[1][iev] ,pick_smear_y[2][iev],pick_smear_y[3][iev],pick_smear_y[4][iev],pick_smear_y[5][iev],
          pick_smear_kappa[0][iev],pick_smear_kappa[1][iev],pick_smear_kappa[2][iev],pick_smear_kappa[3][iev],pick_smear_kappa[4][iev],pick_smear_kappa[5][iev] ,
          pick_phi[0][iev],pick_phi[1][iev],pick_phi[2][iev],pick_phi[3][iev],pick_phi[4][iev],pick_phi[5][iev])
        max_mass_now=max_offshellness(shifted_event)
        if lt(max_mass_now,max_mass) and is_physical(shifted_event,ecm):
          found_shift=True
          max_mass = max_mass_now
          min_offshell_event = shifted_event

      elif idd==3 and idd<=order:
        shifted_event += 1./2.*1./3.*m.pow(dlambda,3.) * d3_evtgen(hardevent, name, ecm, k_lambda, LambdaQCD, -1,
          pick_smear_y[0][iev] ,pick_smear_y[1][iev] ,pick_smear_y[2][iev] ,pick_smear_y[3][iev] ,pick_smear_y[4][iev] ,pick_smear_y[5][iev] ,
          pick_smear_kappa[0][iev] ,pick_smear_kappa[1][iev] ,pick_smear_kappa[2][iev] ,pick_smear_kappa[3][iev] ,pick_smear_kappa[4][iev] ,pick_smear_kappa[5][iev] ,
          pick_phi[0][iev], pick_phi[1][iev], pick_phi[2][iev], pick_phi[3][iev], pick_phi[4][iev], pick_phi[5][iev])
        max_mass_now=max_offshellness(shifted_event)
        if lt(max_mass_now,max_mass) and is_physical(shifted_event,ecm):
          found_shift=True
          max_mass = max_mass_now
          min_offshell_event = shifted_event

      elif idd==4 and idd<=order:
        shifted_event += 1./2.*1./3.*1./4.*m.pow(dlambda,4.) * d4_evtgen(hardevent, name, ecm, k_lambda, LambdaQCD, -1,
          pick_smear_y[0][iev] ,pick_smear_y[1][iev] ,pick_smear_y[2][iev] ,pick_smear_y[3][iev] ,pick_smear_y[4][iev] ,pick_smear_y[5][iev] ,
          pick_smear_kappa[0][iev] ,pick_smear_kappa[1][iev] ,pick_smear_kappa[2][iev] ,pick_smear_kappa[3][iev] ,pick_smear_kappa[4][iev] ,pick_smear_kappa[5][iev] ,
          pick_phi[0][iev], pick_phi[1][iev], pick_phi[2][iev], pick_phi[3][iev], pick_phi[4][iev], pick_phi[5][iev])
        max_mass_now=max_offshellness(shifted_event)
        if lt(max_mass_now,max_mass) and is_physical(shifted_event,ecm):
          found_shift=True
          max_mass = max_mass_now
          min_offshell_event = shifted_event

      elif idd==5 and idd<=order:
        shifted_event += 1./2.*1./3.*1./4.*1./5.*m.pow(dlambda,5.) * d5_evtgen(hardevent, name, ecm, k_lambda, LambdaQCD, -1,
          pick_smear_y[0][iev] ,pick_smear_y[1][iev] ,pick_smear_y[2][iev] ,pick_smear_y[3][iev] ,pick_smear_y[4][iev] ,pick_smear_y[5][iev] ,
          pick_smear_kappa[0][iev] ,pick_smear_kappa[1][iev] ,pick_smear_kappa[2][iev] ,pick_smear_kappa[3][iev] ,pick_smear_kappa[4][iev] ,pick_smear_kappa[5][iev] ,
          pick_phi[0][iev], pick_phi[1][iev], pick_phi[2][iev], pick_phi[3][iev], pick_phi[4][iev], pick_phi[5][iev])
        max_mass_now=max_offshellness(shifted_event)
        if lt(max_mass_now,max_mass) and is_physical(shifted_event,ecm):
          found_shift=True
          max_mass = max_mass_now
          min_offshell_event = shifted_event

      elif idd==6 and idd<=order:
        shifted_event += 1./2.*1./3.*1./4.*1./5.*1./6.*m.pow(dlambda,6.) * d6_evtgen(hardevent, name, ecm, k_lambda, LambdaQCD, -1,
          pick_smear_y[0][iev] ,pick_smear_y[1][iev] ,pick_smear_y[2][iev] ,pick_smear_y[3][iev] ,pick_smear_y[4][iev] ,pick_smear_y[5][iev] ,
          pick_smear_kappa[0][iev] ,pick_smear_kappa[1][iev] ,pick_smear_kappa[2][iev] ,pick_smear_kappa[3][iev] ,pick_smear_kappa[4][iev] ,pick_smear_kappa[5][iev] ,
          pick_phi[0][iev], pick_phi[1][iev], pick_phi[2][iev], pick_phi[3][iev], pick_phi[4][iev], pick_phi[5][iev])
        max_mass_now=max_offshellness(shifted_event)
        if lt(max_mass_now,max_mass) and is_physical(shifted_event,ecm):
          found_shift=True
          max_mass = max_mass_now
          min_offshell_event = shifted_event
      else:
        if found_shift:
          #print ("Warning: Giving up - rescaling least off-shell kinematics to obey on-shell constraints. Shifted lambda=", (k_lambda+dlambda)*LambdaQCD)
          if rescue:
            min_offshell_event = reshuffle(min_offshell_event, ecm)
        else:
          #print ("Error: Giving up - no viable shifted event could be found. Shifted lambda=", (k_lambda+dlambda)*LambdaQCD)
          # No rescue. Set weight to zero below
          if rescue:
            min_offshell_event = evolvedEvent
        break

    shifted_event = min_offshell_event
    shifted_evt = convertArrayToEvent(shifted_event)
    wt = shifted_event[-1][1]
    if not found_shift and rescue:
      wt = 0.0
    shifted_lhe[i].update(wt*weight,hardevent,shifted_evt)  

    if print_c:
      controlEvent=getEvolvedEvent(hardevent, name, ecm, k_lambda+dlambda, LambdaQCD, 1,
        pick_smear_y[0][iev] ,pick_smear_y[1][iev] ,pick_smear_y[2][iev] ,pick_smear_y[3][iev] ,pick_smear_y[4][iev] ,pick_smear_y[5][iev] ,
        pick_smear_kappa[0][iev] ,pick_smear_kappa[1][iev] ,pick_smear_kappa[2][iev] ,pick_smear_kappa[3][iev] ,pick_smear_kappa[4][iev] ,pick_smear_kappa[5][iev] ,
        pick_phi[0][iev], pick_phi[1][iev], pick_phi[2][iev], pick_phi[3][iev], pick_phi[4][iev], pick_phi[5][iev])
      wt2 = controlEvent[-1][1]
      control_evt=convertArrayToEvent(controlEvent)
      lhe[i].update(wt2*weight,hardevent,control_evt)

# Done with generation. Write LHE files
for lhef in lhe:
  lhef.finalize()
for slhef in shifted_lhe:
  slhef.finalize()

